from unittest import TestCase
from unittest.mock import patch

import MockExample
__author__ = 'ja'

def question_function_mock_(*args, **kwargs ):
        return 1

class TestPrint_result(TestCase):

    @patch('MockExample.Question.ultimat_question_of_universe_and_everything', return_value=42)
    def test_print_result(self, mock_question):
        expected_msg = "The answer is 42"
        self.assertEqual(MockExample.print_result(),expected_msg)

    @patch('MockExample.Question.ultimat_question_of_universe_and_everything', side_effect=question_function_mock_)
    def test_side_effect(self, mock_question):
        expected_msg = "The answer is 1"
        self.assertEqual(MockExample.print_result(),expected_msg)

    @patch('MockExample.Question.ultimat_question_of_universe_and_everything', side_effect=ValueError)
    def test_side_effect_raise(self, mock_question):
        expected_msg = "The answer is 1"
        self.assertRaises(ValueError,MockExample.print_result)


