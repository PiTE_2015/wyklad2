from unittest import TestCase
import BasicCalculator

__author__ = 'ja'


class TestBasicCalculator(TestCase):

    def test_should_add_return_correct_sum_for_integers_as_input(self):
        calculator = BasicCalculator.BasicCalculator()
        first_number = 2
        second_number = 2
        expected_sum = 4
        self.assertEqual(calculator.add(first_number, second_number), expected_sum)

    def test_should_add_return_correct_sum_for_floats_as_input(self):
        calculator = BasicCalculator.BasicCalculator()
        first_number = 2.1
        second_number = 2.2
        expected_sum = 4.3
        self.assertAlmostEqual(calculator.add(first_number, second_number), expected_sum)

    def test_should_raise_exception_when_input_is_not_a_number(self):
        calculator = BasicCalculator.BasicCalculator()
        input_not_number = "aaa"
        input_number = 2
        self.assertRaises(ValueError, calculator.add, input_not_number, input_number)
        self.assertRaises(ValueError, calculator.add, input_number, input_not_number)
        self.assertRaises(ValueError, calculator.add, input_not_number, input_not_number)


if __name__ == '__main__':
    TestCase.main()