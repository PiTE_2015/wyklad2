__author__ = 'ja'

class BasicCalculator:
    def add(self, first_number, second_number):
        if not isinstance(first_number, (int, float)) or not isinstance(second_number, (int, float)):
            raise ValueError
        return first_number+second_number
